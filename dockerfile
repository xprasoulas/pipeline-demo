FROM ubuntu:18.04
RUN apt-get update && apt-get install -y gawk wget git-core diffstat unzip

RUN groupadd -g 1000 dev \
  && useradd -u 1000 -g dev -d /home/dev dev \
  && mkdir /home/dev \
  && chown -R dev:dev /home/dev



ENV LANG en_US.UTF-8

USER dev

WORKDIR /home/dev
