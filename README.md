# pipeline-demo
Usually need to run

vagrant up
from the folder that vagrantfile is,
but VirtualBox bug "watchdog bug soft lockup" when 2 CPU is assign to master
wan't let the automation of creation a K8s Cluster. So we create it "step by step"

List all vms

vagrant status

Current machine states:

master                    running (virtualbox)
node01                    not created (virtualbox)
node02                    not created (virtualbox)

Then Run

vagrant up master

After finishing the installation shutdown the VM and change the CPU to 1(K8s need while installation min 2 CPU)

Power on master

Then run

vagrant up node01

when it finished shut it down

vagrant halt node01

and  run

vagrant up node02

when it finished power on node01 and the cluster is ready

vagrant ssh master

to check if the cluster is working ok run

kubectl get nodes
e.g.


NAME            STATUS   ROLES                  AGE     VERSION       
master-node     Ready    control-plane,master   6h32m   v1.23.6       
worker-node01   Ready    worker                 6h22m   v1.23.6 

expecting two nodes but one node is not ready

to check if the cluster is connected run

kubectl cluster-info

e.g.
Kubernetes control plane is running at https://10.0.0.10:6443
CoreDNS is running at https://10.0.0.10:6443/api/v1/namespaces/kube-system/services/kube-dns:dns/proxy

To further debug and diagnose cluster problems, use 'kubectl cluster-info dump'.

Install at master

sudo snap install helm --classic

and register (or connect) the K8s cluster to gitlab through WebUI (section infrastructure-->kubernetes clusters-->connect a cluster):

helm repo add gitlab https://charts.gitlab.io
helm repo update
helm upgrade --install pipeline-demo gitlab/gitlab-agent \
    --namespace gitlab-agent \
    --create-namespace \
    --set image.tag=v15.1.0 \
    --set config.token=$TOKEN_GITLAB \
    --set config.kasAddress=wss://kas.gitlab.com

if registered correctly:

Release "pipeline-demo" does not exist. Installing it now.
NAME: pipeline-demo
LAST DEPLOYED: Wed Sep 14 14:56:12 2022
NAMESPACE: gitlab-agent
STATUS: deployed
REVISION: 1
TEST SUITE: None

create a namespace for the runners

kubectl create namespace gitlab-runner-space

if created correctly:

namespace/gitlab-runner-space created

create a folder gitlab-runner on master node and place the files

mkdir gitlab-runner

cd  gitlab-runner

sudo nano gitlab-runner-secret.yaml
sudo nano gitlab-runner-values.yaml

on gitlab-runner-secret.yaml paste the runner registration token as found on gitlab web page

e.g.

stringData:
  runner-registration-token: "GR134894119UTAcPfUxnFZ2n7cyzJ"
  runnerRegistrationToken: "GR134894119UTAcPfUxnFZ2n7cyzJ"

Apply the secret after changing the namespace in it and using the right runner-registration-token

kubectl apply -f ./gitlab-runner-secret.yaml

you will see this:

secret/gitlab-runner created

helm install --namespace gitlab-runner-space gitlab-runner -f gitlab-runner-values.yaml gitlab/gitlab-runner


you will see this

NAME: gitlab-runner
LAST DEPLOYED: Wed Sep 14 15:13:58 2022
NAMESPACE: gitlab-runner-space
STATUS: deployed
REVISION: 1
TEST SUITE: None
NOTES:
Your GitLab Runner should now be registered against the GitLab instance reachable at: "https://www.gitlab.com/"

to make changes only run the following:

helm upgrade --namespace gitlab-runner-space gitlab-runner -f gitlab-runner-values.yaml gitlab/gitlab-runner


to check tha namespaces run 

kubectl get namespace

NAME                   STATUS   AGE
default                Active   3h13m
gitlab-agent           Active   13m
gitlab-runner-space    Active   10m
kube-node-lease        Active   3h13m
kube-public            Active   3h13m
kube-system            Active   3h13m
kubernetes-dashboard   Active   3h13m

to check the pods inside a namespace run 

kubectl -n gitlab-runner-space get pods

to check if it is ok you should see:

NAME                             READY   STATUS    RESTARTS   AGE
gitlab-runner-648c7d49b9-gp7rr   1/1     Running   0          97s



to check logs inside a pod run 

kubectl -n gitlab-runner-space logs gitlab-runner-648c7d49b9-gp7rr

Registration attempt 1 of 30
Runtime platform                                    arch=amd64 os=linux pid=13 revision=bbcb5aba version=15.3.0
WARNING: Running in user-mode.
WARNING: The user-mode requires you to manually start builds processing: 
WARNING: $ gitlab-runner run
WARNING: Use sudo for system-mode:
WARNING: $ sudo gitlab-runner...

Configuration (with the authentication token) was saved in "/home/gitlab-runner/.gitlab-runner/config.toml" 
Runtime platform                                    arch=amd64 os=linux pid=6 revision=bbcb5aba version=15.3.0
Starting multi-runner from /home/gitlab-runner/.gitlab-runner/config.toml...  builds=0
WARNING: Running in user-mode.                     
WARNING: Use sudo for system-mode:                 
WARNING: $ sudo gitlab-runner...  


to get the serviceaccount run:

kubectl get serviceaccount -n gitlab-runner-space

e.g
NAME            SECRETS   AGE
default         1         15d
gitlab-runner   1         15d

to get the yaml output run:
kubectl get serviceaccount -n gitlab-runner-space -o yaml

e.g
apiVersion: v1
items:
- apiVersion: v1
  kind: ServiceAccount
  metadata:
    creationTimestamp: "2022-09-14T14:57:19Z"
    name: default
    namespace: gitlab-runner-space
    resourceVersion: "2797"
    uid: 843b2869-f9e9-49de-880a-bcfd9a57027d
  secrets:
  - name: default-token-tbclx
- apiVersion: v1
  kind: ServiceAccount
  metadata:
    annotations:
      meta.helm.sh/release-name: gitlab-runner
      meta.helm.sh/release-namespace: gitlab-runner-space        
    creationTimestamp: "2022-09-14T15:13:58Z"
    labels:
      app: gitlab-runner
      app.kubernetes.io/managed-by: Helm
      chart: gitlab-runner-0.44.0
      heritage: Helm
      release: gitlab-runner
    name: gitlab-runner
    namespace: gitlab-runner-space
    resourceVersion: "3919"
    uid: 29c994af-474e-41e2-935b-ebde82bee44f
  secrets:
  - name: gitlab-runner-token-fjqzl
kind: List
metadata:
  resourceVersion: ""
  selfLink: ""


to get the secret run:

kubectl get secret gitlab-runner-token-fjqzl -n gitlab-runner-space -o yaml

apiVersion: v1
data:
  ca.crt: LS0tLS1CRUdJTiBDRVJUSUZJQ0FURS0tLS0tCk1JSUMvakNDQWVhZ0F3SUJBZ0lCQURBTkJna3Foa2lHOXcwQkFRc0ZBREFWTVJNd0VRWURWUVFERXdwcmRXSmwKY201bGRHVnpNQjRYRFRJeU1Ea3hOREUwTXpBMU0xb1hEVE15TURreE1URTBNekExTTFvd0ZURVRNQkVHQTFVRQpBeE1LYTNWaVpYSnVaWFJsY3pDQ0FTSXdEUVlKS29aSWh2Y05BUUVCQlFBRGdnRVBBRENDQVFvQ2dnRUJBTGJpCnFzR3Z0cG9lZ2Q0SWR4bm5MT2dNZy9HNDFnbGlmMGl6SmJPWU9oVU11RmUrOWd5MUl3VFdkbUk0T21zMlQrQlIKc3ZMQWZROW84eDlXTzhvQUpKMTFEYmxPZTNTc2E0aEdLMmJWWG1FTE9NREZhS3RpcXhxTkJFeElLMElNbzJtMgptZElMeWNRVHFUaWFkWWRCQnBBRXhFS09PLzYxcmE4UUI5NndwWUk5Sm53aFo0Y2dYVnVNSWEwQVFQeVlMYVp5CkhqdUN0dHg5YmxaL25RTmUwOVRRSDcvMjRaZGFhTVArcnpaRmVGSU4ycmRjaW4zS0htRkVZeEROczVDOVcyY0YKaXpBTjBtc3JXU25SNEJTRTlGbndoUmFPN2xJeiswcXNOOHh5ZUptU3lNNU91WXVDUXJVZTZ0RFd2WFUvYVAwQwpCL0pxMDVTZHJsS0xQY3BqQldNQ0F3RUFBYU5aTUZjd0RnWURWUjBQQVFIL0JBUURBZ0trTUE4R0ExVWRFd0VCCi93UUZNQU1CQWY4d0hRWURWUjBPQkJZRUZEWHBJSnhLQjc4N1kzTDlHZzc5ZWNSYmkzcmtNQlVHQTFVZEVRUU8KTUF5Q0NtdDFZbVZ5Ym1WMFpYTXdEUVlKS29aSWh2Y05BUUVMQlFBRGdnRUJBRXJNVVZ4bklYM1VPTnhDWVlPSgpBOGJkZG9FbC9ld2tGTm0zKzVPTVpZV3BQNkhJa2VNLy9NWUx2MGN1K0Qzcms3YS81R20zTW00VmtCRmJtTkY3CmFQR1RCY05vS3FVMXZJWXdmbmZOb09RZTZ3YVBvVXMzanBMcXNCUDNrUXdIcDF6dTRHNUQyZ2RjVldLb0txcGUKRmpSYnpKc3I5SkMyNk0zWDJ0NnIxNUFoWEpkTllmU0FLK1hlaWFFdzJTbXpyTE1MUFpxOXAyeUdNK292VU16OApvdGp5SXpNZURMUWRmZHAzTWNoRytZWitURk9TOHh2cFVYcnJQSWJoR1dXK0VjbXFoOERwOVVwUlFaN0lLTXZFCkk5Kzlib0UwckR1WktRbWRCOXYxb3hWWHlCRmU4YjJkYkROd2lhY2pPNTE3UUU1dHFlblA2L0RZc0RxMDZBYVEKVklvPQotLS0tLUVORCBDRVJUSUZJQ0FURS0tLS0tCg==
  namespace: Z2l0bGFiLXJ1bm5lci1zcGFjZQ==
  token: ZXlKaGJHY2lPaUpTVXpJMU5pSXNJbXRwWkNJNkltcDBabmc1VUdOME1teHZZbXBwVjBsRU1HTlRjRTlIWTNWaVoyRlpTVEpJVW1KamFGQlViVWN0YlRRaWZRLmV5SnBjM01pT2lKcmRXSmxjbTVsZEdWekwzTmxjblpwWTJWaFkyTnZkVzUwSWl3aWEzVmlaWEp1WlhSbGN5NXBieTl6WlhKMmFXTmxZV05qYjNWdWRDOXVZVzFsYzNCaFkyVWlPaUpuYVhSc1lXSXRjblZ1Ym1WeUxYTndZV05sSWl3aWEzVmlaWEp1WlhSbGN5NXBieTl6WlhKMmFXTmxZV05qYjNWdWRDOXpaV055WlhRdWJtRnRaU0k2SW1kcGRHeGhZaTF5ZFc1dVpYSXRkRzlyWlc0dFptcHhlbXdpTENKcmRXSmxjbTVsZEdWekxtbHZMM05sY25acFkyVmhZMk52ZFc1MEwzTmxjblpwWTJVdFlXTmpiM1Z1ZEM1dVlXMWxJam9pWjJsMGJHRmlMWEoxYm01bGNpSXNJbXQxWW1WeWJtVjBaWE11YVc4dmMyVnlkbWxqWldGalkyOTFiblF2YzJWeWRtbGpaUzFoWTJOdmRXNTBMblZwWkNJNklqSTVZems1TkdGbUxUUTNOR1V0TkRGbE1pMDVNelZpTFdWaVpHVTRNbUpsWlRRMFppSXNJbk4xWWlJNkluTjVjM1JsYlRwelpYSjJhV05sWVdOamIzVnVkRHBuYVhSc1lXSXRjblZ1Ym1WeUxYTndZV05sT21kcGRHeGhZaTF5ZFc1dVpYSWlmUS50QnQtckdkdWc5dG1Yd0NSWFdVMDhoUXpZaHpxRHV0VjM1TjctQkFEdGtjeW1lZEdSV2IxWGJiUDl3SHlIdWtRa0xMbnY4emloREcyMmowQ1R6MGRLRFB5MHNuUEhvNGppaDF3ZGM2VWV5LWJBQTE3dFFNNkpSb0pGSzlYZVo2U2kxd2FtWGx0X2Rya3lEd2dtV0ZkYWtqUUNqRnpmNnNLczYwc3FCRS0za21HU2pHWlZ0Ulk0ZmZ1ZHBPc3pGZk5pN2dmUlg3Mms5cnF3U0pKbUx6YWtaeFF4LTAtcjR5b01sRUVyLWpQeWtnaTd1aldSb0tmXzJsNVNZenk3ZmhUZlhiRlBhdFVtUEF5V1pjODFpUS0xcnpGUVpYdENXV21uNEN0VHM3Y21NZ0pDNGdrNk14VXVlVlVPUlBSY3ZLd0RCT2ZFX3psNnduWUdScF9VWVRyRGc=
kind: Secret
metadata:
  annotations:
    kubernetes.io/service-account.name: gitlab-runner
    kubernetes.io/service-account.uid: 29c994af-474e-41e2-935b-ebde82bee44f
  creationTimestamp: "2022-09-14T15:13:58Z"
  name: gitlab-runner-token-fjqzl
  namespace: gitlab-runner-space
  resourceVersion: "3917"
  uid: 54f3b6ef-393f-4aa2-8e84-df8019da92cf
type: kubernetes.io/service-account-token


to get the service run

kubectl get service -n gitlab-runner-space

e.g
No resources found in gitlab-runner-space namespace.


if you have deployed a service then

kubectl get service -n gitlab-runner-space

NAME       TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)    AGE
frontend   ClusterIP   10.105.77.132   <none>        3000/TCP   11m


kubectl get deployment -n gitlab-runner-space
NAME            READY   UP-TO-DATE   AVAILABLE   AGE
frontend        1/1     1            1           13m
gitlab-runner   1/1     1            1           15d



to access the app
kubectl port-forward service/frontend -n gitlab-runner-space 3000:3000
Forwarding from 127.0.0.1:3000 -> 3000
Forwarding from [::1]:3000 -> 3000

if you run
kubectl get all -n gitlab-runner-space

NAME                                 READY   STATUS    RESTARTS   AGE
pod/gitlab-runner-76b6c66964-xxx   1/1     Running   0          6m38s

NAME                            READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/gitlab-runner   1/1     1            1           6m38s

NAME                                       DESIRED   CURRENT   READY   AGE
replicaset.apps/gitlab-runner-xxxxxxxxxxx   1         1         1       6m38s


to delete the namespace and all of the resources

kubectl delete all --all -n gitlab-runner-space

check status

kubectl -n gitlab-runner-space get pods

If the status is on terminating do:

kubectl get namespace <YOUR_NAMESPACE> -o json > tmp.json

e.g.

kubectl get namespace gitlab-runner-space -o json > tmp.json

remove from spec  the "finalizers" and its settings


sudo nano tmp.json

 "spec": {
        "finalizers": [
            "kubernetes"
        ]
    },

kubectl replace --raw "/api/v1/namespaces/<YOUR_NAMESPACE>/finalize" -f ./tmp.json


e.g.

kubectl replace --raw "/api/v1/namespaces/gitlab-runner-space/finalize" -f ./tmp.json

Monitoring

create folder,and copy file master.sh

mkdir monitoring

Run the ./monitoring/master.sh

or instead run the commands inside the master.sh file

kubectl -n monitoring get pods

kubectl port-forward --address 0.0.0.0 prometheus-grafana-yyy-xxxx -n monitoring 3000:3000

hostname -I

Grafan UI
10.0.0.10:3000

