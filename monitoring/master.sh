#!/bin/bash
#
# Setup monitoring for Cluster servers

sudo snap install helm --classic &&
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts &&
helm repo update &&

kubectl create namespace monitoring &&
helm install prometheus prometheus-community/kube-prometheus-stack --namespace monitoring
# admin pasword: prom-operator


kubectl create namespace gitlab-runner-space
